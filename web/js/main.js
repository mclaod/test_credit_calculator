$(function () {

    $('.js-credit-calc').on('submit', function (e) {
        e.preventDefault();

        return false;
    });
    $(document).on("click", ".js-credit-run", function (e) {

        var triggerObj = $(this);
        $('<div id="loader"><i class="fa fa-circle-o-notch fa-spin" style="font-size:24px"></i> Загрузка...</div>').appendTo(triggerObj.closest('div'));

        $(this).attr("disabled", true);
        $.ajax({
            url: '',
            type: 'POST',
            dataType: 'json',
            data: $('#calc-form').serialize(),
            success: function (data) {

                if (data.status == 'ok') {
                    $('#results').html(data.calculation_result);
                } else {
                    $('#results').empty();
                    $('#calc-form-wrapper').html(data.form);
                }

            },
            error: function (data) {
                $('#results').html('Сервис не доступен. Пожалуйста, повторите запроc позже.');

            },
            complete: function () {
                $('#loader').remove();
                triggerObj.attr("disabled", false);
            }

        });

    });

});
