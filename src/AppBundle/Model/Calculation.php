<?php
/**
 * Created by PhpStorm.
 * User: mclao
 * Date: 20.08.2018
 * Time: 13:30
 */

namespace AppBundle\Model;

class Calculation
{
    const ROUND_LENGTH = 2;
    
    /**
     * @param \AppBundle\Entity\Calculation $inputData
     * @param int $round знак округления
     * @return array
     */
    public function getCalculationResults(\AppBundle\Entity\Calculation $inputData)
    {
        // $period - срок кредита (в месяцах),
        // $percent процентная ставка, $amount - сумма кредита (в рублях)
        // $month - месяц начала выплат,
        // $year - год начала выплат,
        
        $monthList = array_combine(range(1, 12), ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'] );
        
        $amount  = $inputData->getCreditAmount();
        $period  = $inputData->getPeriod();
        $percent = $inputData->getPercent();
        $month = $inputData->getCreditStartMonth();
        $year  = $inputData->getCreditStartYear();
        

        $month_rate = ($percent / 100 / 12);   //  месячная процентная ставка по кредиту (= годовая ставка / 12)
        $k          = ($month_rate * pow((1 + $month_rate), $period)) / (pow((1 + $month_rate), $period) - 1); // коэффициент аннуитета
        $payment    = round($k * $amount, self::ROUND_LENGTH);   // Размер ежемесячных выплат
        $overpay    = ($payment * $period) - $amount;
        $debt       = $amount;

        for ($i = 1; $i <= $period; $i++) {
            $schedule[$i] = [];

            $percent_pay = round($debt * $month_rate, self::ROUND_LENGTH);
            $credit_pay  = round($payment - $percent_pay, self::ROUND_LENGTH);

            $schedule[$i]['month']       = $monthList[$month] . ' ' . $year;
            $schedule[$i]['dept']        = self::moneyFormat($debt);
            $schedule[$i]['percent_pay'] = self::moneyFormat($percent_pay);
            $schedule[$i]['credit_pay']  = self::moneyFormat($credit_pay);
            $schedule[$i]['payment']     = self::moneyFormat($payment);

            $debt = $debt - $credit_pay;

            if ($month++ >= 12) {
                $month = 1;
                $year++;
            }
        }

        $result = [];

        $result['overpay'] = self::moneyFormat($overpay);
        $result['payment'] = self::moneyFormat($payment);
        $result['schedule'] = $schedule;

        return $result;
    }
    
    public static function moneyFormat($val)
    {
          return number_format($val, self::ROUND_LENGTH, ',', ' ');
    }
}
