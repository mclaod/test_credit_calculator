<?php
/**
 * Created by PhpStorm.
 * User: mclao
 * Date: 19.08.2018
 * Time: 19:19
 */

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class CalculationFormType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $current_year = date('Y');
        $yearsList    = range($current_year, $current_year + 5);
        $yearsList    = array_combine($yearsList, $yearsList);

        $monthList = array_combine(['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'], range(1, 12));

        $builder
            ->add('creditAmount', TextType::class, ['attr' => ['class' => 'form-control']])
            ->add('period', TextType::class, ['attr' => ['class' => 'form-control']])
            ->add('percent', TextType::class, ['attr' => ['class' => 'form-control']])
            ->add('creditStartMonth', ChoiceType::class, [
                'choices' => $monthList,
                'attr'    => ['class' => 'form-control'],
            ])
            ->add('creditStartYear', ChoiceType::class, [
                'choices' => $yearsList,
                'attr'    => ['class' => 'form-control'],

            ])
            ->add('save', SubmitType::class, [
                'label' => 'Рассчитать',
                'attr'  => ['class' => 'js-credit-run btn btn-block btn-warning']
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'AppBundle\Entity\Calculation',
            'attr'       => ['novalidate' => 'novalidate']
        ]);
    }
}
