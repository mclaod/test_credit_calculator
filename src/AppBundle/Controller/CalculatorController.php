<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Calculation;
use AppBundle\Form\CalculationFormType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class CalculatorController extends Controller
{
    /**
     * @Route("/", name="calculator")
     */
    public function indexAction(Request $request)
    {
        $calc = new Calculation();
        $calc->setCreditAmount('500000');
        $calc->setPeriod('12');
        $calc->setPercent('8');

        //текущий месяц
        $calc->setCreditStartMonth(date('n'));
        $calc->setCreditStartYear(date('Y'));

        $form = $this->createForm(CalculationFormType::class, $calc);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $calc = $form->getData();

                //Сохраняем данные о расчете в БД
                $em = $this->getDoctrine()->getManager();
                $em->persist($calc);
                $em->flush();

                $model =  new \AppBundle\Model\Calculation();
                $result = $model->getCalculationResults($calc);


                return new JsonResponse([
                        'status' => 'ok',
                        'calculation_result' => $this->renderView('main/result.html.twig',[
                            'result' => $result
                        ])
                    ], 200);
            } else {
                return new JsonResponse([
                    'status' => 'error',
                    'form'   => $this->renderView('main/calc_form.html.twig',
                        [
                            'form' => $form->createView(),
                        ]
                    )], 200);
            }
        }

        return $this->render('main/calculator.html.twig', [
            'form' => $form->createView(),

        ]);
    }
}
