<?php
/**
 * Created by PhpStorm.
 * User: mclao
 * Date: 19.08.2018
 * Time: 18:28
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/*
 * Клиент вводит сумму кредита, указывает срок кредита в месяцах и процентную ставку, дату первого платежа
 * */


/**
 * Class Calculation
 * @ORM\Entity()
 * @package AppBundle\Entity
 */
class Calculation
{


    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Assert\NotBlank(message="Введите сумму кредита.")
     * @Assert\GreaterThan(value=0, message="Введите сумму кредита.")
     * @ORM\Column(type="float")
     */
    private $creditAmount;

    /**
     * @Assert\NotBlank(message="Введите срок кредитования в месяцах")
     * @Assert\GreaterThan(value=0, message="Введите срок кредитования в месяцах")
     * @Assert\LessThanOrEqual(value=36, message="Срок не может превышать {{ compared_value }} месяцев.")
     * @ORM\Column(type="integer")
     */
    private $period;
    /**
     * @Assert\NotBlank()
     * @ORM\Column(type="float")
     */
    private $percent;
    /**
     * @Assert\NotBlank()
     * @Assert\GreaterThan(0)
     * @Assert\LessThanOrEqual(12)
     * @ORM\Column(type="integer")
     */
    private $creditStartMonth;

    /**
     * @Assert\NotBlank()
     * @Assert\GreaterThanOrEqual(2018)
     * @ORM\Column(type="integer")
     */
    private $creditStartYear;

    /**
     * @ORM\Column(type="string")
     */
    private $sessionId;

    /**
     * @ORM\Column(type="string")
     */
    private $userIp;

    /**
     * @ORM\Column(type="datetime")
     */

    private $calcDate;


    public function __construct()
    {
        $this->calcDate = new \DateTime();
        $this->sessionId = session_id();
        $this->userIp = $_SERVER['REMOTE_ADDR'];
    }

    /**
     * @return mixed
     */
    public function getSessionId()
    {
        return $this->sessionId;
    }

    /**
     * @param mixed $sessionId
     */
    public function setSessionId($sessionId)
    {
        $this->sessionId = $sessionId;
    }

    /**
     * @return mixed
     */
    public function getUserIp()
    {
        return $this->userIp;
    }

    /**
     * @param mixed $userIp
     */
    public function setUserIp($userIp)
    {
        $this->userIp = $userIp;
    }

    /**
     * @return mixed
     */
    public function getCreditStartMonth()
    {
        return $this->creditStartMonth;
    }

    /**
     * @param mixed $creditStartMonth
     */
    public function setCreditStartMonth($creditStartMonth)
    {
        $this->creditStartMonth = $creditStartMonth;
    }

    /**
     * @return mixed
     */
    public function getCreditStartYear()
    {
        return $this->creditStartYear;
    }

    /**
     * @param mixed $creditStartYear
     */
    public function setCreditStartYear($creditStartYear)
    {
        $this->creditStartYear = $creditStartYear;
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getCreditAmount()
    {
        return $this->creditAmount;
    }

    /**
     * @param mixed $creditAmount
     */
    public function setCreditAmount($creditAmount)
    {
        $this->creditAmount = (float)$creditAmount;
    }

    /**
     * @return mixed
     */
    public function getPeriod()
    {
        return (int)$this->period;
    }

    /**
     * @param mixed $period
     */
    public function setPeriod($period)
    {
        $this->period = (int)$period;
    }

    /**
     * @return mixed
     */
    public function getPercent()
    {
        return (float)$this->percent;
    }

    /**
     * @param mixed $percent
     */
    public function setPercent($percent)
    {
        $this->percent = (float)str_replace(",", ".", $percent);
    }

    /**
     * @return mixed
     */
    public function getCalcDate()
    {
        return $this->calcDate ?: new \DateTime('now');
    }

    /**
     * @param mixed $calcDate
     */
    public function setCalcDate($calcDate)
    {
        $this->calcDate = $calcDate;
    }


}